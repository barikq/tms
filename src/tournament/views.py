from django.http import HttpResponse

from tournament.models import Tournament


def tournament_detail(request, tournament_id):
    tournament = Tournament.objects.filter(pk=tournament_id).first()

    if not tournament:
        tournament_name = 'Tournament not found'
    else:
        tournament_name = tournament.name

    return HttpResponse(tournament_name)
